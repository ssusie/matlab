clear; close; clc;

load('MultiGNATNormConstr')

[x,y]=meshgrid(2:5,1:25);


plot3(x',y',NormConstr(2:end,:), 'o')
grid on

xlabel('number of subdomains')
ylabel('number of time steps')
zlabel('norm of the GNAT constraint')