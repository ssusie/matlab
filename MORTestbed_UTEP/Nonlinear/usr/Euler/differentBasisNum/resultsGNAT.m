clear; close all; clc;

constrained=load('myGNATNormConstr');
orig=load('origGNATNormConstr');

[x,y]=meshgrid(2:24,1:25);

figure(1)
plot3(x', y', constrained.NormConstr(2:end,:), 'ko', 'linewidth', 2)
zlim([0 0.1])
xlabel('number of basis vectors')
ylabel('timestep')
zlabel('norm of the constrains')
title('Constrained GNAT')
grid on


figure(2)
plot3(x', y', orig.NormConstr(2:end,:), 'ro', 'linewidth', 2)
zlim([0 10])
xlabel('number of basis vectors')
ylabel('timestep')
zlabel('norm of the constrains')
title('Original GNAT')
grid on
  %%
% figure(3)
%  close all;
% for j=2:24
%     
%     plot(1:25,constrained.NormConstr(j,:), 'ko')
%     hold on
%     plot(1:25,orig.NormConstr(j,:), 'r*')
%     ylim([0 2])
%     pause(1) 
%     close 
%     legend('constrained GNAT', 'orgingal GNAT')
% end
%%

relErrOrig=load('RelErrOrigGNAT');
relErrConstr=load('RelErrmyGNAT');
relErrROM=load('RelErrROM');
[x,y]=meshgrid(2:24,1:25);

figure(4)
plot3(x,y,relErrROM.RelErrROM(2:end,2:end), 'b')
hold on,grid on
plot3(x,y,relErrOrig.RelErrGNAT(2:end,2:end), 'r')
plot3(x,y,relErrConstr.RelErrGNAT(2:end,2:end), 'k')
xlabel('number of basis vectors')
ylabel('relative error for each case')
zlim([0 0.1])
% legend('rom', 'orgingal GNAT', 'constrained GNAT')


%%

meanRom = mean(relErrROM.RelErrROM,2);
meanGnatOrig = mean(relErrOrig.RelErrGNAT,2);
meanGnatConstr = mean(relErrConstr.RelErrGNAT,2);


normRom = ColumnwiseNorm(relErrROM.RelErrROM');
normGnatOrig = ColumnwiseNorm(relErrOrig.RelErrGNAT');
normGnatConstr = ColumnwiseNorm(relErrConstr.RelErrGNAT');

figure(5)
subplot(2,1,1)
plot(3:24,normRom(3:end), 'b--', 'linewidth', 2)
hold on
plot(3:24,normGnatOrig(3:end), 'r--','linewidth', 2)
plot(3:24,normGnatConstr(3:end), 'k','linewidth', 2)
xlim([3 24])
xlabel('number of basis vectors')
ylabel('norm of the error')
title('norm of relative error at each time step')
legend('rom', 'orgingal GNAT', 'constrained GNAT')
subplot(2,1,2)
plot(3:24,meanRom(3:end), 'b--', 'linewidth', 2)
hold on
plot(3:24,meanGnatOrig(3:end), 'r--','linewidth', 2)
plot(3:24,meanGnatConstr(3:end), 'k','linewidth', 2)
xlim([3 24])
xlabel('number of basis vectors')
ylabel('mean of rel er')
title('mean of relative error')
legend('rom', 'orgingal GNAT', 'constrained GNAT')


