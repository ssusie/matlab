clear; close; clc;

load('MultiGNATNormConstr')
load('RelErrMultiGNAT')
load('RelErrRom')


%%
[x,y]=meshgrid(2:4,1:25);

figure(1)
plot3(x',y',NormConstr(2:end,:), 'o')
grid on

xlabel('number of subdomains')
ylabel('number of time steps')
zlabel('norm of the GNAT constraint')

%%
figure(2)
plot3(x', y', RelErrGNAT(2:4,2:end), 'o')
grid on
xlabel('number of subdomains')
ylabel('number of time steps')

figure(3)
plot3(x', y', RelErrROM(2:4,2:end), 'o')
grid on
xlabel('number of subdomains')
ylabel('number of time steps')


%%

figure(2)
plot3(x', y', RelErrGNAT(2:4,2:end), 'ok')
hold on
plot3(x', y', RelErrROM(2:4,2:end), 'or')
grid on
xlabel('number of subdomains')
ylabel('number of time steps')







