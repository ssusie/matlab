clear; close all; clc;

extensions = {'99','999','9999'};
%  extensions = {'99'};
fom=load('solnfom');

figureIndex = 1;

for iExtension = 1:length(extensions)
	Gal = load(['romG',extensions{iExtension}]);
	PetGal = load(['romPG',extensions{iExtension}]);
	LSPG = load(['romZim',extensions{iExtension}]);
    fminconROM=load(['romfmincon',extensions{iExtension}]);
    gnatOrig=load(['GnatOrig',extensions{iExtension}]);
    gnatReal=load(['GnatZimReal',extensions{iExtension}]);
    gnatFmincon=load(['GnatFmin',extensions{iExtension}]);
    
    normErrEachTimeStepGal=ColumnwiseNorm(Gal.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepPetGal=ColumnwiseNorm(PetGal.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepLSPG=ColumnwiseNorm(LSPG.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepfmincon=ColumnwiseNorm(fminconROM.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    
    normErrEachTimeStepGnatOrig=ColumnwiseNorm(gnatOrig.svG-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepGnatZimReal=ColumnwiseNorm(gnatReal.svG-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepGnatFminconReal=ColumnwiseNorm(gnatFmincon.svG-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    
%     h1=figure(figureIndex);
%     figureIndex = figureIndex + 1;
%     plot(normErrEachTimeStepGal, 'g*', 'linewidth',2); hold on
%     plot(normErrEachTimeStepPetGal, 'r*', 'linewidth',2);
%     plot(normErrEachTimeStepLSPG, 'b*', 'linewidth',2); 
%     plot(normErrEachTimeStepfmincon, 'yo', 'linewidth',2); 
%     plot(normErrEachTimeStepGnatOrig, 'k*', 'linewidth',2); 
%     plot(normErrEachTimeStepGnatZimReal, 'c*', 'linewidth',2); hold on
%     plot(normErrEachTimeStepGnatFminconReal, 'mo', 'linewidth',2);
%     legend('G','PG','LSPG','fminconROM' ,'GnatOrig', 'GnatZimReal', 'GnatFmincon')
%     title(['norm of state space error between rom and fom,  ' , extensions{iExtension}, '%'])
%     xlabel('timestep')
%     ylabel('relative error')
%     print(['errorROM_FOM_dots', extensions{iExtension}],'-deps')
    
    h1=figure(figureIndex);
    figureIndex = figureIndex + 1;
    semilogy(normErrEachTimeStepGal, 'g', 'linewidth',2); hold on
    semilogy(normErrEachTimeStepPetGal, 'r', 'linewidth',2);
    semilogy(normErrEachTimeStepLSPG, 'b-o', 'linewidth',2); 
    semilogy(normErrEachTimeStepfmincon, 'y-*', 'linewidth',2); 
    semilogy(normErrEachTimeStepGnatOrig, 'k', 'linewidth',2); 
    semilogy(normErrEachTimeStepGnatZimReal, 'c-*', 'linewidth',2);
    semilogy(normErrEachTimeStepGnatFminconReal, 'm-o', 'linewidth',2);
    legend('G','PG','PGreal','fminconROM', 'Gnat', 'GnatReal', 'GnatRealFmincon')
    title(['log of the norm of the state space error between rom and fom  ', extensions{iExtension}, '%'])
    xlabel('timestep')
    ylabel('relative error')
%     print(['errorROM_FOM', extensions{iExtension}],'-depsc')
    
    h1=figure(figureIndex);
    figureIndex = figureIndex + 1;
	semilogy(abs(Gal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'g--', 'linewidth',2); hold on
	semilogy(abs(PetGal.rom.sv(4:3:end-3,end)-fom.soln(4:3:end-3,end)), 'r--','linewidth',2); hold on
	semilogy(abs(LSPG.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'b--', 'linewidth',2);
    semilogy(abs(fminconROM.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'y-', 'linewidth',2);
    semilogy(abs(gnatOrig.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'k--', 'linewidth',2);
    semilogy(abs(gnatReal.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'c-', 'linewidth',2);
    semilogy(abs(gnatFmincon.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'm--', 'linewidth',2);
	legend('G','PG','PGreal','fminconROM','Gnat', 'GnatReal', 'GnatRealFmincon')
	title(['log of the error for rho at final time step for case   ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel('log of value of error for rho')
%     print(['logErrorRho', extensions{iExtension}],'-depsc')

	h2=figure(figureIndex);
	figureIndex = figureIndex + 1;
	semilogy(abs(Gal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'g--', 'linewidth',2); hold on
	semilogy(abs(PetGal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'r--','linewidth',2); hold on
	semilogy(abs(LSPG.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'b--', 'linewidth',2);
    semilogy(abs(fminconROM.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'y-', 'linewidth',2);
	semilogy(abs(gnatOrig.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'k--', 'linewidth',2);
    semilogy(abs(gnatReal.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'c-', 'linewidth',2);
    semilogy(abs(gnatFmincon.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'm--', 'linewidth',2);
	legend('G','PG','PGreal','fminconROM','Gnat', 'GnatReal', 'GnatRealFmincon')
	title(['log of the error for rho*u at final time step   ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel('log of value of the error for rho*u')
%     print(['logErrRhoU', extensions{iExtension}],'-depsc')

	h3=figure(figureIndex);
	figureIndex = figureIndex + 1;
    semilogy(abs(Gal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'g--', 'linewidth',2); hold on
	semilogy(abs(PetGal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'r--','linewidth',2); hold on
	semilogy(abs(LSPG.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'b--', 'linewidth',2);
    semilogy(abs(fminconROM.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'y-', 'linewidth',2);
	semilogy(abs(gnatOrig.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'k--', 'linewidth',2);
    semilogy(abs(gnatReal.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'c-', 'linewidth',2);
    semilogy(abs(gnatFmincon.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'm--', 'linewidth',2);
	legend('G','PG','PGreal','fminconROM','Gnat', 'GnatReal', 'GnatRealFmincon')
	title(['log of the error for e at final time step    ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel('log of value of the error for e')
%     print(['logErrE', extensions{iExtension}],'-depsc')

end
