clear; close all; clc;

% extensions = {'99','999','9999'};
extensions = {'999'};
fom=load('solnfom');

figureIndex = 1;

for iExtension = 1:length(extensions)
	Gal = load(['romG',extensions{iExtension}]);
	PetGal = load(['romPG',extensions{iExtension}]);
	PGreal = load(['romZim',extensions{iExtension}]);
    
    gnat=load(['GnatOrig',extensions{iExtension}]);
    gnatReal=load(['GnatZimReal',extensions{iExtension}]);
    
    normErrEachTimeStepGal=ColumnwiseNorm(Gal.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepPG=ColumnwiseNorm(PetGal.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepPGreal=ColumnwiseNorm(PGreal.rom.sv-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    
    normErrEachTimeStepGnat=ColumnwiseNorm(gnat.svG-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    normErrEachTimeStepGnatReal=ColumnwiseNorm(gnatReal.svG-fom.soln,2)./ColumnwiseNorm(fom.soln,2);
    
    h1=figure(figureIndex);
    figureIndex = figureIndex + 1;
    plot(normErrEachTimeStepGal, 'g', 'linewidth',2); hold on
    plot(normErrEachTimeStepPG, 'r', 'linewidth',2);
    plot(normErrEachTimeStepPGreal, 'b-o', 'linewidth',2); 
    
    plot(normErrEachTimeStepGnat, 'k', 'linewidth',2); 
    plot(normErrEachTimeStepGnatReal, 'm', 'linewidth',2);
    
    legend('G','PG','PGreal', 'Gnat', 'GnatReal')
    title(['norm of state space error between rom and fom  ', extensions{iExtension}, '%'])
    xlabel('timestep')
    ylabel('relative error')
%     
%     h1=figure(figureIndex);
%     figureIndex = figureIndex + 1;
%     plot(log(normErrEachTimeStepGal), 'g', 'linewidth',2); hold on
%     plot(log(normErrEachTimeStepPG), 'r', 'linewidth',2);
%     plot(log(normErrEachTimeStepPGreal), 'b-o', 'linewidth',2); 
%     
%     plot(log(normErrEachTimeStepGnat), 'k', 'linewidth',2); 
%     plot(log(normErrEachTimeStepGnatReal), 'm', 'linewidth',2);
%     
%     legend('G','PG','PGreal', 'Gnat', 'GnatReal')
%     title(['norm of state space error between rom and fom  ', extensions{iExtension}, '%'])
%     xlabel('timestep')
%     ylabel('log relative error')
%     
%     h1=figure(figureIndex);
%     figureIndex = figureIndex + 1;
%     plot(log(1+normErrEachTimeStepGal), 'g', 'linewidth',2); hold on
%     plot(log(1+normErrEachTimeStepPG), 'r', 'linewidth',2);
%     plot(log(1+normErrEachTimeStepPGreal), 'b-o', 'linewidth',2); 
%     
%     plot(log(1+normErrEachTimeStepGnat), 'k', 'linewidth',2); 
%     plot(log(1+normErrEachTimeStepGnatReal), 'm', 'linewidth',2);
%     
%     legend('G','PG','PGreal', 'Gnat', 'GnatReal')
%     title(['norm of state space error between rom and fom  ', extensions{iExtension}, '%'])
%     xlabel('timestep')
%     ylabel('log 1+relative error')
    
    
    h1=figure(figureIndex);
    figureIndex = figureIndex + 1;
    subplot(2,1,1)
	plot(abs(Gal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'g--', 'linewidth',2); hold on
	plot(abs(PetGal.rom.sv(4:3:end-3,end)-fom.soln(4:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'b--', 'linewidth',2);

    plot(abs(gnat.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'm--', 'linewidth',2);

	legend('G','PG','PGreal','Gnat', 'GnatReal')
	title([' error for rho at final time step for case   ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel('error for rho')
    subplot(2,1,2)
	plot(abs(PetGal.rom.sv(4:3:end-3,end)-fom.soln(4:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'b--', 'linewidth',2);

    plot(abs(gnat.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'm--', 'linewidth',2);

	legend('PG','PGreal','Gnat', 'GnatReal')
	title('plot on the top without Galerkin')
    xlabel('space x')
    ylabel('error for rho')
    
    
%     h1=figure(figureIndex);
%     figureIndex = figureIndex + 1;
% 	semilogy(abs(Gal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'g--', 'linewidth',2); hold on
% 	semilogy(abs(PetGal.rom.sv(4:3:end-3,end)-fom.soln(4:3:end-3,end)), 'r--','linewidth',2); hold on
% 	semilogy(abs(PGreal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'b--', 'linewidth',2);
% 
%     semilogy(abs(gnat.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'k--', 'linewidth',2);
%     semilogy(abs(gnatReal.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end)), 'm--', 'linewidth',2);
% 
% 	legend('G','PG','PGreal','Gnat', 'GnatReal')
% 	title(['log of the error for rho at final time step for case   ',extensions{iExtension}, '%'])
%     xlabel('space x')
%     ylabel('log of value of error for rho')
%     
%     h1=figure(figureIndex);
%     figureIndex = figureIndex + 1;
% 	plot(log(abs(Gal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end))), 'g--', 'linewidth',2); hold on
% 	plot(log(abs(PetGal.rom.sv(4:3:end-3,end)-fom.soln(4:3:end-3,end))), 'r--','linewidth',2); hold on
% 	plot(log(abs(PGreal.rom.sv(4:3:end-3,end)- fom.soln(4:3:end-3,end))), 'b--', 'linewidth',2);
% 
%     plot(log(abs(gnat.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end))), 'k--', 'linewidth',2);
%     plot(log(abs(gnatReal.svG(4:3:end-3,end)- fom.soln(4:3:end-3,end))), 'm--', 'linewidth',2);
% 
% 	legend('G','PG','PGreal','Gnat', 'GnatReal')
% 	title(['log of the error for rho at final time step for case   ',extensions{iExtension}, '%'])
%     xlabel('space x')
%     ylabel('log of value of error for rho')


	h2=figure(figureIndex);
	figureIndex = figureIndex + 1;
    subplot(2,1,1)
	plot(abs(Gal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'g--', 'linewidth',2); hold on
	plot(abs(PetGal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'b--', 'linewidth',2);

	plot(abs(gnat.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'm--', 'linewidth',2);

	legend('G','PG','PGreal', 'Gnat', 'GnatReal')
	title(['error for rho*u at final time step   ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel('error for rho*u')
    subplot(2,1,2)
	plot(abs(PetGal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'b--', 'linewidth',2);

	plot(abs(gnat.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(5:3:end-3,end)- fom.soln(5:3:end-3,end)), 'm--', 'linewidth',2);

	legend('PG','PGreal', 'Gnat', 'GnatReal')
	title('plot on the top without Galerkin')
    xlabel('space x')
    ylabel('error for rho*u')


	h3=figure(figureIndex);
	figureIndex = figureIndex + 1;
    subplot(2,1,1)
    plot(abs(Gal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'g--', 'linewidth',2); hold on
	plot(abs(PetGal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'b--', 'linewidth',2);

	plot(abs(gnat.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'm--', 'linewidth',2);

	legend('G','PG','PGreal', 'Gnat', 'GnatReal')
	title(['error for e at final time step    ',extensions{iExtension}, '%'])
    xlabel('space x')
    ylabel(' error for e')

    subplot(2,1,2)
	plot(abs(PetGal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'r--','linewidth',2); hold on
	plot(abs(PGreal.rom.sv(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'b--', 'linewidth',2);

	plot(abs(gnat.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'k--', 'linewidth',2);
    plot(abs(gnatReal.svG(6:3:end-3,end)- fom.soln(6:3:end-3,end)), 'm--', 'linewidth',2);

	legend('PG','PGreal', 'Gnat', 'GnatReal')
	title('plot on the top without Galerkin')
    xlabel('space x')
    ylabel(' error for e')


end
