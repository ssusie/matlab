clear; close all; clc;
%  extensions = {'99','999','9999'};
  extensions = {'999'};

figureIndex = 1;
for iExtension = 1:length(extensions)
	Gal = load(['Gconstr',extensions{iExtension}]);
	PetGal = load(['PGconstr',extensions{iExtension}]);
	LSPG = load(['Zimconstr',extensions{iExtension}]);
    ROMfmincon=load(['romfminconConstr',extensions{iExtension}]);
        
    gnatOrig_real= load(['OrigGnatRealConstr',extensions{iExtension}]);
    gnatOrig_approx=load(['OrigGnatApproxConstr',extensions{iExtension}]);
    gnatZimReal_real=load(['GnatZimReal_realConstr',extensions{iExtension}]);
    gnatZimReal_approx=load(['GnatZimReal_approxConstr',extensions{iExtension}]);
    gnatfminconreal=load(['GnatFminRealconstr',extensions{iExtension}]);
    
    
    disp(['Frob norm of constraines for Galerkin      ', num2str(norm(Gal.constr, 'fro'))])
    disp(['Frob norm of constraines for PG     ', num2str(norm(PetGal.constr, 'fro'))])
    disp(['Frob norm of constraines for LSPG     ', num2str(norm(LSPG.constr, 'fro'))])
    disp(['Frob norm of constraines for ROMfmincon     ', num2str(norm(ROMfmincon.constr, 'fro'))]) 
    disp(['Frob norm of constraines for GNAT_original, real constr     ', num2str(norm(gnatOrig_real.Rconstr, 'fro'))]) 
    disp(['Frob norm of constraines for GNAT_original, approx constr     ', num2str(norm(gnatOrig_approx.Aconstr, 'fro'))]) 
    disp(['Frob norm of constraines for GNAT_Zim with realConstr, real constr     ', num2str(norm(gnatZimReal_real.Rconstr, 'fro'))]) 
    disp(['Frob norm of constraines for GNAT_Zim with realConstr, approx constr     ', num2str(norm(gnatZimReal_approx.Aconstr, 'fro'))]) 
    disp(['Frob norm of constraines for GNAT_fmincon real constrs     ', num2str(norm(gnatfminconreal.constr, 'fro'))]) 
    
    
    normEachTime_Gal=ColumnwiseNorm(Gal.constr,2);
    normEachTime_PG=ColumnwiseNorm(PetGal.constr,2);
    normEachTime_LSPG=ColumnwiseNorm(LSPG.constr,2);
    normEachTime_romFmincon=ColumnwiseNorm(ROMfmincon.constr,2);
    
    normEachTime_GnatorigReal=ColumnwiseNorm(gnatOrig_real.Rconstr,2);
    normEachTime_GnatorigApprox=ColumnwiseNorm(gnatOrig_approx.Aconstr,2);
    normEachTime_GnatZimreal_real=ColumnwiseNorm(gnatZimReal_real.Rconstr,2);
    normEachTime_GnatZimreal_approx=ColumnwiseNorm(gnatZimReal_approx.Aconstr,2);
    normEachTime_GnatZimfminconReal=ColumnwiseNorm(gnatfminconreal.constr,2);
    

    figure(figureIndex)
    figureIndex=figureIndex+1;
    subplot(2,1,1)
    plot(normEachTime_Gal,'g*','linewidth',2), hold on
    plot(normEachTime_PG,'r*','linewidth',2)
    plot(normEachTime_LSPG,'b*','linewidth',2)
    plot(normEachTime_romFmincon,'yv','linewidth',2)
    legend('G','PG','LSPG','fminconROM' )
    title(['Norm of constraints for ROM    ',extensions{iExtension}])
%     xlabel('time step')
    ylabel('2-norm of  constraints')
%     print(['ylimconstrRom', extensions{iExtension}],'-depsc')
    subplot(2,1,2)
    plot(normEachTime_Gal,'g*','linewidth',2), hold on
    plot(normEachTime_PG,'r*','linewidth',2)
    plot(normEachTime_LSPG,'b*','linewidth',2)
    plot(normEachTime_romFmincon,'yv','linewidth',2)
    legend('G','PG','LSPG','fminconROM' )
    title(['zoom in'])
    xlabel('time step')
%     ylabel('2-norm of  constraints at each time step')
    ylim([0 1])
    
    figure(figureIndex)
    figureIndex=figureIndex+1;
    subplot(2,1,1)
    plot(normEachTime_GnatorigReal,'k*','linewidth',2), hold on
    plot(normEachTime_GnatorigApprox,'b*','linewidth',2)
    plot(normEachTime_GnatZimreal_real,'r*','linewidth',2)
    plot(normEachTime_GnatZimreal_approx,'mv','linewidth',2)
    plot(normEachTime_GnatZimfminconReal,'yo','linewidth',2)
    legend('GnatOrig, real constr', 'GnatOrig, approx constr',...
        'GnatZimReal, real constr','GnatZimReal, approx constr','GnatFmincon')
    ylabel('2-norm of  constraints')
    title(['Norm of constraints for GNAT   ',extensions{iExtension}])
    subplot(2,1,2)
    plot(normEachTime_GnatorigReal,'k*','linewidth',2), hold on
    plot(normEachTime_GnatorigApprox,'b*','linewidth',2)
    plot(normEachTime_GnatZimreal_real,'r*','linewidth',2)
    plot(normEachTime_GnatZimreal_approx,'mv','linewidth',2)
    plot(normEachTime_GnatZimfminconReal,'yo','linewidth',2)
    ylim([0 0.1])
    xlabel('time step')
%     print(['constrGnat', extensions{iExtension}],'-depsc')
    
end
