clear; close all; clc;

constrained=load('myGNATNormConstr');
orig=load('NormConstr');

[x,y]=meshgrid(2:24,1:25);

figure(1)
plot3(x', y', constrained.NormConstr(2:end,:), 'ko', 'linewidth', 2)
zlim([0 10])
xlabel('number of basis vectors')
ylabel('timestep')
zlabel('norm of the constrains')
title('Constrained GNAT')
grid on


figure(2)
plot3(x', y', orig.NormConstr(2:end,:), 'ro', 'linewidth', 2)
zlim([0 10])
xlabel('number of basis vectors')
ylabel('timestep')
zlabel('norm of the constrains')
title('Original GNAT')
grid on
 %%
figure(3)
% close all;
for j=2:24
    
    plot(1:25,constrained.NormConstr(j,:), 'ko')
    hold on
    plot(1:25,orig.NormConstr(j,:), 'r*')
    ylim([0 2])
    pause(1) 
    close
    legend('constrained GNAT', 'orgingal GNAT')
end
%%

relErrOrig=load('RelErrGNATorig');
relErrConstr=load('RelErrmyGNAT');
relErrROM=load('RelErrROM');

figure(4)
plot(2:24,relErrROM.RelErrROM(2:end), 'bv')
xlabel('number of basis vectors')
ylabel('relative error for each case')
hold on
plot(2:24,relErrOrig.RelErrGNAT(2:end), 'r*')
plot(2:24,relErrConstr.RelErrGNAT(2:end), 'ko')
ylim([0 1])
legend('rom', 'orgingal GNAT', 'constrained GNAT')












