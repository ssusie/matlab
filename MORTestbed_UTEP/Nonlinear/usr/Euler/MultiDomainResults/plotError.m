clear; close all; clc;

LSPG=load('LSPGFrobnError');
Reg=load('RegFnError');
load('F') % frobanius norm of the fom soln

LSPGcnstr=load('LSPGConstrNorm');
Regcnstr=load('RegConstrNorm');

LSPGcol=load('LSPGColNorm');
Regcol=load('RegColNorm');
[x,y]=meshgrid(0:10, 2:25);

%%
for i=1:25
    for j=1:10
        CN(i,j)=mean(squeeze(LSPGcol.colNorm(i,j,:)));
    end
    CN0(i,:)=mean(Regcol.colNorm(i,:));
end
C=[CN0, CN];

figure(1)
plot3(x',y',C(2:end,:),'o')
xlabel('3*constriants')
ylabel('number of basis vectors')
zlabel('mean of the relative error')
grid on
zlim([0 0.1])
%%

figure(2)
plot3(x',y',[Reg.FrobnError(2:end)/F, LSPG.FrobnError(2:end,:)/F], 'o'),

zlim([0 0.1]),
grid on
xlabel('3*constriants')
ylabel('number of basis vectors')
zlabel('frob norm of the error')
title('frob norm of the error over frob norm of fom')
%%
% close
N=zeros(25,10);
for i=2:25
    for j=1:10
        N(i, j)=norm(squeeze(LSPGcnstr.Cnorm(i,j,:)));
        
    end
    regN(i,:)=norm(squeeze(Regcnstr.Cnorm(i,:)));
end

X=[regN,N];
%%
figure(3)
plot3(x',y',X(2:end,:), 'o')
grid on
zlim([0 0.1])
xlabel('3*constriants')
ylabel('number of basis vectors')
title('norm of the constraint for each case')







